# README #

1/3スケールのNEC PC-8801mk2風小物のstlファイルです。
スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。
元ファイルはAUTODESK 123D DESIGNです。

***

# 実機情報

## メーカ
- NEC

## 発売時期
- 1983年11月

## 参考資料

- [Wikipedia](https://ja.wikipedia.org/wiki/PC-8800%E3%82%B7%E3%83%AA%E3%83%BC%E3%82%BA)
- [ボクたちが愛した、想い出のパソコン・マイコンたち](https://akiba-pc.watch.impress.co.jp/docs/column/retrohard/1068992.html)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_pc-8801mk2/raw/3fc6ebde04e5472096896c9e5037bb7168d351b2/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_pc-8801mk2/raw/3fc6ebde04e5472096896c9e5037bb7168d351b2/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_pc-8801mk2/raw/3fc6ebde04e5472096896c9e5037bb7168d351b2/ExampleImage.png)
